#pragma once

//This is the header you need to include if you simply want to include the whole library.
//Using this is the recommended way over including the individual headers since it is the most tested way.

#include "sec_sigc_base_include.hpp"

#include "sec_trackable.hpp"
#include "sec_signal.hpp"

