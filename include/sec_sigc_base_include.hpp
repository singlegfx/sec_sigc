#pragma once

#ifndef SEC_SIGC_EXPORT_MACRO
#define SEC_SIGC_EXPORT_MACRO
#endif

/**
 * The core namespace that contains all classes of this library
 */
namespace sec_sigc{}

/**
 * This namespace contains internal implementation details that are not part of the public API.
 * @warning Do not use anything from this namespace, it is not part of the public API and may change at any time.
 */
namespace sec_sigc::internal{}
