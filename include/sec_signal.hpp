#pragma once

#include "sec_trackable.hpp"

#include <type_traits>

namespace sec_sigc {

    /**
     * @brief A simple class representing a function object.
     * @tparam RET_TYPE
     * @tparam FUNC_ARGS
     */
    template <class RET_TYPE, class... FUNC_ARGS>
    class func_obj {
      public:
        virtual RET_TYPE operator()(FUNC_ARGS... args) = 0;
    };

    /**
     * @brief An abstraction over the tracked weak_ptr that is used to connect to a tracked_func_obj.
     * This only exists to make the code more readable.
     * It wraps the tracked_func_obj and provides a callable operator that calls the tracked_func_obj.
     * If the tracked_func_obj is no longer alive, the callable operator will return std::nullopt.
     * This removes the need to convert function objects returned from weak_track_ptr to a callable function object.
     *
     * @tparam RET_TYPE
     * @tparam FUNC_ARGS
     */
    template <class RET_TYPE, class... FUNC_ARGS>
    class weak_func_track_ptr : public weak_track_ptr<func_obj<RET_TYPE, FUNC_ARGS...>> {
      private:
        std::function<RET_TYPE(FUNC_ARGS...)> functor;

        weak_func_track_ptr(std::function<RET_TYPE(FUNC_ARGS...)> _fun, uint64_t id)
            : weak_track_ptr<func_obj<RET_TYPE, FUNC_ARGS...>>{id},
              functor{_fun} {}

      public:
        static weak_func_track_ptr<RET_TYPE, FUNC_ARGS...> create(std::function<RET_TYPE(FUNC_ARGS...)> fun, uint64_t id) {
            return weak_func_track_ptr<RET_TYPE, FUNC_ARGS...>{fun, id};
        }

        template <class OBJ_TYPE>
            requires std::is_base_of_v<sec_trackable, OBJ_TYPE>
        static weak_func_track_ptr<RET_TYPE, FUNC_ARGS...> create(std::function<RET_TYPE(FUNC_ARGS...)> fun, OBJ_TYPE& tracked_obj) {
            return weak_func_track_ptr<RET_TYPE, FUNC_ARGS...>{fun, tracked_obj.sec_getID()};
        }

        template <class passed_obj_type>
            requires std::invocable<passed_obj_type, FUNC_ARGS...> &&
                     std::same_as<std::invoke_result_t<passed_obj_type, FUNC_ARGS...>, RET_TYPE> &&
                     std::is_base_of_v<sec_trackable, passed_obj_type>
        static weak_func_track_ptr<RET_TYPE, FUNC_ARGS...> create(const passed_obj_type& tracked_obj) {
            auto id = tracked_obj.sec_getID();

            passed_obj_type*                      tracked_ptr = const_cast<passed_obj_type*>(&tracked_obj);
            std::function<RET_TYPE(FUNC_ARGS...)> converted_obj = [tracked_ptr](FUNC_ARGS... args) {
                return tracked_ptr->operator()(args...);
            };

            return weak_func_track_ptr<RET_TYPE, FUNC_ARGS...>{converted_obj, id};
        }

        /**
         * @brief Calls the tracked_func_obj.
         *
         * This is a wrapper around the call operator of the tracked_func_obj.
         * If the tracked_func_obj is no longer alive, this function will return std::nullopt.
         * If an exception is thrown during the call of the tracked_func_obj, this function will also return std::nullopt.
         * Otherwise, it will return the return value of the tracked_func_obj.
         * This design choice was made to prevent segfaults and to make the code more readable.
         * The exception handling is done here to prevent errors from spreading to the caller of this function.
         * It is assumed that an exception during the call should prevent further calls to the tracked_func_obj.
         * So make sure that you handle all exceptions in the functor and only throw exceptions if the functor should be unregistered.
         * @warning if the RET_TYPE is void, this function will return std::optional<bool> instead of std::optional<void>.
         * This is a workaround to get over the fact that std::optional<void> is not allowed.
         * @param args The function arguments that should be passed to the tracked_func_obj.
         * @return empty optional if the tracked_func_obj should be unregistered, otherwise the return value of the tracked_func_obj.
         */
        auto operator()(FUNC_ARGS&... args) noexcept {
            using RETURN_TYPE = std::conditional_t<std::is_void_v<RET_TYPE>, std::optional<bool>, std::optional<RET_TYPE>>;

            if (this->should_delete()) {
                return RETURN_TYPE{std::nullopt};
            }

            try {
                if constexpr (std::is_void_v<RET_TYPE>) {
                    functor(args...);
                    return RETURN_TYPE{true};
                } else {
                    return RETURN_TYPE{functor(args...)};
                }
            } catch (...) {
                return RETURN_TYPE{std::nullopt};
            }
        }
    };

    /**
     * @brief The secure signal class.
     * This is the core class for the function signal system of this library.
     * It is thread safe and can be used to register callback functions that should be called when a signal is emitted.
     * Once a callback function is registered, it will be called every time the signal is emitted.
     * If a connected object is destroyed, the callback function will no longer be called and removed automatically.
     * This class should prevent many types of segfaults and memory leaks while still being easy to use.
     *
     * @note you can add and remove callbacks during the emit phase, but they will only be added or removed before the next emit.
     * This design choice was made to prevent a lot of deadlock scenarios and the deferred processing drastically reduces the amount of
     * mutex lock operations.
     *
     * @note Just like noted in sec_trackable, make sure that your functors are actually thread safe.
     * If their call operator is callable during the destruction of the functor, you might get a segfault.
     * It might be a good idea to manually unregister functors in the destructor of your class.
     *
     * @tparam RET_TYPE
     * @tparam FUNC_ARGS
     */
    template <class RET_TYPE, class... FUNC_ARGS>
    class sec_signal {
      private:
        std::vector<weak_func_track_ptr<RET_TYPE, FUNC_ARGS...>> slots_list;
        std::shared_mutex                                        slots_list_mutex;

        std::vector<weak_func_track_ptr<RET_TYPE, FUNC_ARGS...>> slots_add_queue;
        std::shared_mutex                                        slots_add_mutex;

        std::vector<uint64_t> slots_remove_queue;
        std::shared_mutex     slots_remove_mutex;

      public:
        sec_signal() = default;
        ~sec_signal() = default;

        /**
         * @brief Connects a tracked_func_obj to the signal.
         * @tparam passed_obj_type The type of the tracked_func_obj that should be connected.
         * It needs to inherit from both func_obj and sec_trackable.
         * @param obj The tracked_func_obj that should be connected.
         */
        template <class... CREATE_ARGS>
        void connect(CREATE_ARGS&... obj) {
            connect(weak_func_track_ptr<RET_TYPE, FUNC_ARGS...>::create(obj...));
        }

        void connect(weak_func_track_ptr<RET_TYPE, FUNC_ARGS...> new_slot) {
            std::scoped_lock lock{slots_add_mutex};
            slots_add_queue.emplace_back(new_slot);
        }

        /**
         * @brief Mark a callback function as disconnected.
         * @param id The id of the tracked object that should be disconnected.
         */
        void disconnect(uint64_t id) {
            std::scoped_lock lock{slots_remove_mutex};
            slots_remove_queue.emplace_back(id);
        }

        /**
         * @brief Processes the queues and adds or removes the callback functions.
         */
        void process_queues() {
            // add new slots
            {
                std::scoped_lock lock{slots_add_mutex, slots_list_mutex};
                for (auto& slot : slots_add_queue) {
                    slots_list.emplace_back(slot);
                }
                slots_add_queue.clear();
            }

            // remove queued slots
            // this iterates over the registered slots in the outer loop since the assuption is that the slots list is much larger than the
            // remove queue because of this less data is iterated over in the inner loop. also the remove shrinks faster relative to the
            // reduction in size of the slots list
            std::scoped_lock lock{slots_remove_mutex, slots_list_mutex};
            for (auto it = slots_list.begin(); it != slots_list.end();) {
                auto id = it->getID();
                auto found_it = std::find(slots_remove_queue.begin(), slots_remove_queue.end(), id);
                if (found_it != slots_remove_queue.end()) {
                    it = slots_list.erase(it);
                    slots_remove_queue.erase(found_it);
                } else {
                    it++;
                }

                if (slots_remove_queue.empty()) {
                    break;
                }
            }
            slots_remove_queue.clear();
        }

        /**
         * @brief Emits the signal and calls all connected callback functions.
         * @param args The arguments that should be passed to the callback functions.
         * @warning If RET_TYPE is void, this function will return void instead of std::vector<void>.
         * @return std::vector<RET_TYPE> A vector containing the return values of all callback functions that were called.
         */
        auto emit(FUNC_ARGS... args) {
            // before emitting the signal, we need to process the queues
            process_queues();

            // now lock the slots list and call all callback functions
            std::shared_lock lock{slots_list_mutex};

            // create a collection of the return values of the callback functions
            // if the return type is void, this will be a vector of bools however it will stay empty
            // This is done to prevent the need for a lot of if constexpr statements
            using COLLECTION_TYPE = std::conditional_t<std::is_void_v<RET_TYPE>, std::vector<bool>, std::vector<RET_TYPE>>;
            COLLECTION_TYPE ret_collection;

            // reserve the size of the collection if the return type is not void
            if constexpr (!std::is_void_v<RET_TYPE>) {
                ret_collection.reserve(slots_list.size());
            }

            // iterate over all callback functions and call them
            for (auto slot_it = slots_list.begin(); slot_it != slots_list.end();) {
                auto retval = slot_it->operator()(args...);
                if (retval.has_value()) {
                    // only append to the return vector if the return type is not void
                    if constexpr (!std::is_void_v<RET_TYPE>) {
                        ret_collection.emplace_back(retval.value());
                    }
                    slot_it++;
                } else {
                    slot_it = slots_list.erase(slot_it);
                    continue;
                }
            }

            // return void if RET_TYPE is void, return the collection otherwise
            if constexpr (std::is_void_v<RET_TYPE>) {
                return;
            } else {
                return ret_collection;
            }
        }
    };
} // namespace sec_sigc
