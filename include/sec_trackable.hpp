#pragma once

#include "sec_sigc_base_include.hpp"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <functional>
#include <future>
#include <memory>
#include <mutex>
#include <optional>
#include <shared_mutex>
#include <thread>
#include <utility>
#include <vector>

namespace sec_sigc {
    class sec_trackable;

    namespace internal {
        /**
         * @brief This class is used to track the lifetime of all sec_trackable objects.
         * @warning It is not part of the public API and should not be used directly.
         * Use the sec_trackable class to interact with this class.
         */
        class secure_obj_tracker {
            // allow sec_trackable to access the private functions of this class.
            // sec_trackable needs to be able to register and unregister itself and provides the public stable interface to this class
            friend class ::sec_sigc::sec_trackable;

          private:
            /**
             * @brief A vector containing all the ids of the objects that are currently being tracked.
             * @note these are the only ones considered to be alive.
             */
            std::vector<uint64_t> tracked_objects;
            std::shared_mutex     tracked_objects_mutex;

            /**
             * @brief The next id to use for object tracking.
             * @note use fetch_add to get the next id.
             * The ids returned by that might not be in order but that does not matter.
             * @see track() for how this is used.
             */
            std::atomic<uint64_t> next_id = 1;

            /**
             * @brief The constructor of this class.
             * This is private since this class is a singleton and should only be created once.
             * Use the getInstance function to get the singleton instance of this class.
             */
            secure_obj_tracker();

            /**
             * @brief The destructor of this class.
             * It does not do anything since this class should only be destroyed when the program is exiting.
             */
            ~secure_obj_tracker();

            /**
             * @brief Updates the list of tracked objects and checks if a given id is still alive.
             * @param id The id to check.
             * @note This function might take longer to return than expected, since it might be waiting for other threads to finish.
             * @return True if the id is still alive, false otherwise.
             */
            [[nodiscard]]
            bool alive(uint64_t id);

            /**
             * @brief Track a new object.
             * This creates an id for the object and adds it to the list of tracked objects.
             * Use that id to check if the object is still alive.
             * @note The ids are not being reclaimed and are simply incremented. This means the ids will overflow if you need more than
             * 2^64. It is unlikely to reach that point but it is still something to keep in mind.
             * @return The new unique id of the object.
             */
            [[nodiscard]]
            uint64_t track();

            /**
             * @brief No longer track a given id.
             * This adds an id to the remove list and marks it as no longer being tracked.
             * This means that the id will no longer be considered alive.
             *
             * @param id The id to untrack.
             */
            void untrack(uint64_t id);

            /**
             * @brief Get the singleton instance of this class.
             * @return the singleton instance of this class.
             */
            static secure_obj_tracker& getInstance() {
                static secure_obj_tracker instance;
                return instance;
            }
        };
    } // namespace internal

    /**
     * @brief This class is the base class for all objects that need their lifetime to be tracked by the library.
     * This class is a base class and your type should inherit (public) from this class.
     * Unlike sigc::trackable this class is fully thread safe and should not cause any deadlocks.
     * The only this you need to do is make sure no other code is accessing this object while it is being destroyed.
     */
    class sec_trackable {
      private:
        // The id of this object, if this is 0 the object is no longer being tracked.
        std::atomic<uint64_t> id = 0;

      protected:
        void sec_track() noexcept;
        void sec_untrack() noexcept;

      public:
        /**
         * @brief Construct a new sec_trackable object.
         * This will automatically register this object with the library and assign it an id.
         * The id can be retrieved with the getID function and should be used to check if the object is still alive.
         */
        sec_trackable() noexcept;

        /**
         * @brief Destroy the sec_trackable object.
         */
        ~sec_trackable();

        /**
         * @brief Get the id of this object.
         * Use the id to check if this object is still alive.
         * @return The ID of this object.
         */
        [[nodiscard]]
        uint64_t sec_getID() const noexcept;

        /**
         * @brief A shorthand for isAlive(getID()).
         * @return true if the object is still alive, false otherwise.
         */
        [[nodiscard]]
        bool sec_isAlive() const noexcept;

        /**
         * @brief Check if a given id is still alive.
         * @param id The id to check.
         * @return true if the id is still alive, false otherwise.
         */
        static bool sec_isAlive(uint64_t id) { return internal::secure_obj_tracker::getInstance().alive(id); }
    };

    /**
     * @brief This object abstracts the access to a pointer of a sec_trackable object.
     * It is recommended to use this instead of a raw pointer, since it will automatically check if the pointer is still valid.
     * In case one of the get functions does not return a valid pointer, this object should be discarded.
     * @note This object does not take ownership of the pointer, it is still the responsibility of the caller to delete the object and
     * keep it alive as long as needed.
     * @warning Do NOT store the pointer returned by the get functions, since it may become invalid at any time. It is recommended to
     * directly discard the returned pointer after a single use. It is recommended to use the run_on function and pass a lambda to it.
     * @tparam T The type of the object to track (needs to be derived from sec_trackable).
     */
    template <class T>
    class [[maybe_unused]] weak_track_ptr {
      protected:
        /// The raw pointer to the object.
        T* ptr;
        /**
         * @brief The id of the object.
         * It is set in the constructor and const to prevent it from being changed.
         */
        uint64_t id;

        weak_track_ptr(uint64_t _id) noexcept
            : ptr{nullptr},
              id{_id} {}

      public:
        /**
         * @brief Construct a new weak_track_ptr object.
         * @param obj A raw pointer to the object to track.
         */
        template <class R>
            requires std::is_base_of_v<sec_trackable, R> && std::is_convertible_v<R*, T*>
        [[maybe_unused]]
        explicit weak_track_ptr(R* obj) noexcept
            : ptr{obj},
              id{obj->sec_getID()} {

            if (obj == nullptr || ptr == NULL) {
                throw std::runtime_error("weak_track_ptr: pointer is nullptr");
            }
        }

        /**
         * @brief Construct a new weak_track_ptr object.
         * @param obj A reference to the object to track.
         */
        template <class R>
            requires std::is_base_of_v<sec_trackable, R> && std::is_convertible_v<R*, T*>
        [[maybe_unused]]
        explicit weak_track_ptr(R& obj) noexcept
            : ptr{&obj},
              id{obj.sec_getID()} {}

        /**
         * @brief Construct a new weak_track_ptr object.
         * @param obj A raw pointer to the object to track.
         */
        template <class R>
            requires std::is_convertible_v<R*, T*>
        [[maybe_unused]]
        explicit weak_track_ptr(R* obj, uint64_t _id) noexcept
            : ptr{obj},
              id{_id} {}

        weak_track_ptr(const weak_track_ptr<T>& other) = default;

        weak_track_ptr(weak_track_ptr<T>&& other) noexcept
            : ptr{other.ptr},
              id{other.id} {};

        weak_track_ptr& operator=(const weak_track_ptr<T>& other) {
            if (this == &other) {
                return *this;
            }

            ptr = other.ptr;
            id = other.id;
            return *this;
        }
        weak_track_ptr& operator=(weak_track_ptr<T>&& other) noexcept {
            if (this == &other) {
                return *this;
            }

            ptr = other.ptr;
            id = other.id;
            return *this;
        }


        [[nodiscard]]
        uint64_t getID() const noexcept {
            return id;
        }

        /**
         * @brief A simple check to see if the pointer held by this object is still valid.
         * If this returns true you can discard this object, since it no longer has any use.
         * @return True if the pointer is no longer valid, false otherwise.
         */
        [[nodiscard]]
        bool should_delete() const noexcept {
            return !sec_trackable::sec_isAlive(id);
        }

        /**
         * @brief Run a function on the pointer if it is still valid.
         * @param func The function to run on the pointer.
         * @note This is the recommended way to use this object, since it discourages storing the pointer and allows for easy validation if
         * this object is still valid at the same time.
         * @return True if the pointer was still valid, false otherwise.
         */
        [[maybe_unused]]
        bool run_on(std::function<void(T&)> func) const {
            if (should_delete()) {
                return false;
            }
            func(*ptr);
            return true;
        }

        /**
         * @brief Get the pointer wrapped as optional.
         * @return The pointer wrapped as optional, or nullopt if the pointer is no longer valid.
         */
        [[maybe_unused]]
        auto get_opt() const noexcept {
            return should_delete() ? std::nullopt : std::optional<T*>{ptr};
        }

        /**
         * @brief Get the raw pointer.
         * @return The raw pointer, or nullptr if the pointer is no longer valid.
         */
        [[maybe_unused]]
        T* get_raw() const noexcept {
            return should_delete() ? nullptr : ptr;
        }

        /**
         * @brief Get a reference to the object.
         * @return A reference to the object.
         * @throws std::runtime_error if the pointer is no longer valid.
         */
        [[maybe_unused]]
        T& get_ref() const {
            if (should_delete()) {
                throw std::runtime_error("weak_track_ptr: pointer is no longer valid");
            }
            return *ptr;
        }
    };
} // namespace sec_sigc
