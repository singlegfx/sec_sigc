# Secure Sigc++ (sec_sigc++)

A thread safe alternative to sigc++.
This is a C++23 library to combine functional programming with classes without having to worry about the lifetime of your data.
Just like sigc++ it provides tracking and signal types however they are implemented in a thread-safe and (mostly) deadlock free way.
