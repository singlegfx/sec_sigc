#include <gtest/gtest.h>
#include <sec_sigc++.hpp>

using namespace sec_sigc;

class int_functor : public sec_trackable, public func_obj<int, int> {
  public:
    int_functor() = default;
    ~int_functor() = default;

    int operator()(int i) override { return i; }
};

class int_add_functor : public sec_trackable, public func_obj<int, int, int> {
  public:
    int_add_functor() = default;
    ~int_add_functor() = default;

    int operator()(int a, int b) override { return a + b; }
};

class void_functor : public sec_trackable, public func_obj<void, int> {
  public:
    void_functor() = default;
    ~void_functor() = default;

    void operator()(int) override {}
};

class void_functor_empty : public sec_trackable, public func_obj<void> {
  public:
    void_functor_empty() = default;
    ~void_functor_empty() = default;

    void operator()() override {}
};

TEST(sec_signal, connect_int_functor) {
    sec_signal<int, int> sig{};
    int_functor          slot{};

    ASSERT_TRUE(slot.sec_isAlive());
    ASSERT_EQ(slot(1), 1);

    sig.connect(slot);

    auto ret = sig.emit(1);
    ASSERT_EQ(ret.size(), 1);
    ASSERT_EQ(ret[0], 1);

    sig.disconnect(slot.sec_getID());

    ret = sig.emit(1);
    ASSERT_EQ(ret.size(), 0);
}

TEST(sec_signal, connect_int_add_functor) {
    sec_signal<int, int, int> sig{};
    int_add_functor          slot{};

    ASSERT_TRUE(slot.sec_isAlive());
    ASSERT_EQ(slot(1, 2), 3);

    sig.connect(slot);

    auto ret = sig.emit(1, 2);
    ASSERT_EQ(ret.size(), 1);
    ASSERT_EQ(ret[0], 3);

    sig.disconnect(slot.sec_getID());

    ret = sig.emit(1, 2);
    ASSERT_EQ(ret.size(), 0);
}

TEST(sec_signal, connect_void_functor) {
    sec_signal<void, int> sig{};
    void_functor          slot{};

    ASSERT_TRUE(slot.sec_isAlive());

    sig.connect(slot);

    sig.emit(1);

    sig.disconnect(slot.sec_getID());
}

TEST(sec_signal, connect_void_functor_empty) {
    sec_signal<void> sig{};
    void_functor_empty slot{};

    ASSERT_TRUE(slot.sec_isAlive());

    sig.connect(slot);

    sig.emit();

    sig.disconnect(slot.sec_getID());
}
