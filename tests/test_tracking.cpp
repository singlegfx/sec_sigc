#include <gtest/gtest.h>
#include <sec_sigc++.hpp>

using namespace sec_sigc;

class tracking_obj : public sec_trackable {
  public:
    tracking_obj() = default;
    ~tracking_obj() = default;
};

TEST(sec_tracking, simple_tracking) {
    weak_track_ptr<tracking_obj>* wtp;

    {
        tracking_obj obj{};
        wtp = new weak_track_ptr<tracking_obj>{obj};
        ASSERT_FALSE(wtp->should_delete());
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    ASSERT_TRUE(wtp->should_delete());
}
