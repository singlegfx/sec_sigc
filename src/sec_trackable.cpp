#include "sec_trackable.hpp"

// secure_obj_tracker
sec_sigc::internal::secure_obj_tracker::secure_obj_tracker() {
    tracked_objects.reserve(1000);
}

sec_sigc::internal::secure_obj_tracker::~secure_obj_tracker() = default;

bool sec_sigc::internal::secure_obj_tracker::alive(uint64_t id) {
    std::scoped_lock lock{tracked_objects_mutex};
    return std::ranges::contains(tracked_objects, id);
}

uint64_t sec_sigc::internal::secure_obj_tracker::track() {
    uint64_t id = next_id.fetch_add(1, std::memory_order_relaxed);

    std::scoped_lock lock{tracked_objects_mutex};
    tracked_objects.push_back(id);
    return id;
}

void sec_sigc::internal::secure_obj_tracker::untrack(uint64_t id) {
    if (id == 0) {
        return;
    }

    std::scoped_lock lock{tracked_objects_mutex};

    auto it = std::find(tracked_objects.begin(), tracked_objects.end(), id);
    if (it != tracked_objects.end()) {
        tracked_objects.erase(it);
    }
}

// sec_trackable
sec_sigc::sec_trackable::sec_trackable() noexcept {
    sec_track();
}

sec_sigc::sec_trackable::~sec_trackable() {
    sec_untrack();
}

void sec_sigc::sec_trackable::sec_track() noexcept {
    if(id == 0){
        id = internal::secure_obj_tracker::getInstance().track();
    }
}

void sec_sigc::sec_trackable::sec_untrack() noexcept {
    if(id != 0){
        internal::secure_obj_tracker::getInstance().untrack(id);
        id = 0;
    }
}

uint64_t sec_sigc::sec_trackable::sec_getID() const noexcept {
    return id;
}

bool sec_sigc::sec_trackable::sec_isAlive() const noexcept {
    return sec_isAlive(id);
}
